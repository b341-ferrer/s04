class SampleClass():
	def __init__ (self, year):
		self.year = year;

	def show_year(self):
		print(f"The year is {self.year}");

# Create an instance
# myObj = SampleClass(2023)

# print(myObj.year);
# myObj.show_year();

# [SECTION] Fundamental  of OOP
# There are four main fundamentals of OOP
# Encapsulation
# Inheritance
# Polymorphism
# Abstraction

# [SECTION] Encapsulation
# In encapsulation, the attributes of a class will be hidden from other classes
# to achive encapsulation
# 1. Declare the attribute of the a class
# 2. Provide the getter and setter 


class Person():
	def __init__ (self):
		self._name = "John Doe";
		self._age = 0;

	def set_name(self, name):
		self._name = name;

	def set_age(self, age):
		self._age = age;

	def get_name(self):
		print(f"Name of person : {self._name}");

	def get_age(self):
		print(f"Age of person : {self._age}");

# p1 = Person();

# p1.get_name();
# p1.set_name("Bob Doe");
# p1.get_name();
# print (p1._name)

# # Test Case
# p1.set_age(38);
# p1.get_age();

# # [SECTION] Inheritance
# class Employee (Person):
# 	def __init__(self,employeeId):
# 		super().__init__();
# 		self._employeeId = employeeId;

# 	def get_employeeId(self):
# 		print(f"The Employee ID is : {self._employeeId}");

# 	def set_employeeId(self,employeeId):
# 		self._employeeId = employeeId;

# 	def get_details(self):
# 		print(f"{self._employeeId} belongs to {self._name}");

# emp1 = Employee("Emp-001");
# emp1.get_details();

# emp1.set_name("Jane Doe");
# emp1.set_age(40);
# emp1.get_age();

# emp1.get_details();


# Mini exercise
#     1. Create a new class called Student that inherits Person with the additional attributes and methods
#     attributes: Student No, Course, Year Level
#     methods: 
#       get_detail: prints the output "<Student name> is currently in year <year level> taking up <Course>"

class Student (Person):
	def __init__ (self, student_number, course, year_level):
		super().__init__();
		self._student_number = student_number;
		self._course = course;
		self._year_level = year_level;
	# Setter
	def set_studentNo(student_number):
		self._student_number = student_number;

	def set_course(course):
		self._course = course;

	def set_year_level(year_level):	
		self._year_level = year_level;

	# Getter
	def get_detail(self):
		print(f"{self._name} is currently in year {self._year_level} taking up {self._course}");

	def get_studentNo(self):
		print(f"Student number of the student is {self._student_number}");

	def get_course(self):
		print(f"The course of the student is {self._course}");

	def get_year_level(self):
		print(f"The year level of the student is {self._year_level}");


stud1 = Student("001", "python short course", 1);
stud1.set_name("Brandon Smith");
stud1.set_age(18);
stud1.get_detail();


#[SECTION] Polymorphism
# A function can be created that can take any object, allowing for polymorphism
class Admin ():
	def is_admin(self):
		print(True)
	def user_type(self):
		print("Admin User")

class Customer():
	def is_admin(self):
		print(False)
	def user_type(self):
		print("Regular User")

# Defines a test_function that will take an object
def test_function(obj):
	obj.is_admin();
	obj.user_type();

# Instances foor Admin and Customer
user_admin = Admin();
user_customer = Customer();

# Pass the created instances to test function
test_function(user_admin);
test_function(user_customer);
# What happened is that the test_function would call the method of the object passed to it, hence llowing it yo have diffent output based on the object passed

# Polymorphism with Class method
class TeamLead():
	def occupation (self):
		print ("Team Lead");

	def hasAuth (self):
		print (True);

class TeamMember():
	def occupation (self):
		print ("Team Member");

	def hasAuth (self):
		print (False);

tl1 = TeamLead();
tm1 = TeamMember();

for person in (tl1,tm1):
	# access the occupation method of each item
	person.occupation();

# Polymorphism with INheritance

class Zuitt():
	def tracts(self):
		print(f"We are currently offering 3 tracts (developer, pi-shape career, and short courses)");

	def num_of_hours(self):
		print(f"Learn web development in 360 hours!");

class DeveloperCareer (Zuitt):
	# overrides the parent's num_of_hours (method)
	def num_of_hours(self):
		print(f"Learn the basic of web development in 240 hours");

class PiShapeCareer (Zuitt):
	# overrides the parent's num_of_hours (method)
	def num_of_hours(self):
		print(f"Learn skills for no-code add development in 140 hours");

class ShortCourses (Zuitt):
	# overrides the parent's num_of_hours (method)
	def num_of_hours(self):
		print(f"Learn advanced topic web development in 20 hours");

course1 = DeveloperCareer();
course2 = PiShapeCareer();
course3 = ShortCourses();

course1.num_of_hours();
course2.num_of_hours();
course3.num_of_hours();

















